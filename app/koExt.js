

define(["jquery", "knockout","highlight"], function($, ko, hljs) {
  
  ko.bindingHandlers.hlText = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
    },
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).html(ko.unwrap(valueAccessor()));
        console.log(ko.unwrap(valueAccessor()));
         hljs.highlightBlock(element);
    }
};
return ko;
  
  
});


