define([], function() {
    return {
        'langs':[
            {
                value:"php",
                template:
                '{{#each level1}}{{#if desc}}\n\
  /**\n\
{{#each desc}}    * {{this}}\n\
{{/each}}\n\
    */\n\
{{/if}} namespace {{name}}; \n\
{{#each level2}}{{#if desc}}\n\
  /**\n\
{{#each desc}}    * {{this}}\n\
{{/each}}\n\
    */\n\
{{/if}}   class {{name}} {\n\
{{#each level3}}        public function {{name}}() {\n\
\n\
            var $ret =  \"\";\n\
{{#each desc}}\n\
            $ret .= \"{{this}}\";\n\
{{/each}}\n\
            return $ret;\n\
        }\n\
{{/each}}   }\n\
{{/each}}\n\
{{/each}}'
            },
            {value:"javascript",
                template:
                '{{#each level1}}{{#if desc}}\n\
  /**\n\
{{#each desc}}    * {{this}}\n\
{{/each}}\n\
    */\n\
{{/if}} var {{name}} = (function () { \n\
{{#each level2}}{{#if desc}}\n\
  /**\n\
{{#each desc}}    * {{this}}\n\
{{/each}}\n\
    */\n\
{{/if}}   function _{{name}} {\n\
        return { \n\
{{#each level3}}            "{{name}}" : function() {\n\
\n\
                var ret =  \"\";\n\
{{#each desc}}\n\
                ret .= \"{{this}}\";\n\
{{/each}}\n\
                return ret;\n\
                }\n\
            }\n\
{{/each}}   };\n\
{{/each}}\n\
    return {\n\
{{#each level2}}\n\
        {{name}}:_{{name}}\n\
{{/each}}\n\
    };\n\
})();\n\
\n\
{{/each}}'
            },
            {value:"html",
                template:
                '{{#each level1}}{{#if desc}}\n\
  /**\n\
{{#each desc}}    * {{this}}\n\
{{/each}}\n\
    */\n\
{{/if}} namespace {{name}}; \n\
{{#each level2}}{{#if desc}}\n\
  /**\n\
{{#each desc}}    * {{this}}\n\
{{/each}}\n\
    */\n\
{{/if}}   class {{name}} {\n\
{{#each level3}}        public function {{name}}() {\n\
\n\
            var $ret =  \"\";\n\
{{#each desc}}\n\
            $ret .= \"{{this}}\";\n\
{{/each}}\n\
            return $ret;\n\
        }\n\
{{/each}}   }\n\
{{/each}}\n\
{{/each}}'
            }],
        cvs:{level1:[
                {name:"CV",
                    desc:[
                        "Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w mojej aplikacji",
                        "dla potrzeb niezbędnych do realizacji procesów rekrutacji ",
                        "(zgodnie z Ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych tj. Dz. U. z 2002 r., Nr 101, poz. 926, ze zm.)."
                    ],
                    level2:[
                        {
                            name:"PersonalData",
                            level3:[
                                {
                                   name:"Name",
                                   desc:["Krzysztof Bakun"]
                                },
                                {
                                    name:"Address",
                                    desc:["Suwalna 17/13","05-120 Legionowo","Poland"]
                                },
                                {
                                    name:"DateOfBirth",
                                    desc:["02.06.1978"]
                                },
                                {
                                    name:"Contacts",
                                    desc:["+48 506 794 988","krzysztof@bakun.biz","http://kbakun.blogspot.com/","https://twitter.com/bakula_kb"]
                                }
                            ]
                            
                        }
                    ]
                },
                {
                    name:"Education",
                    level2:[
                        { 
                            name:"Study",
                            desc:[
                                "   _____ _             _       ",
                                "  / ____| |           | |      ",
                                " | (___ | |_ _   _  __| |_   _ ",
                                "  \\___ \\| __| | | |/ _` | | | |",
                                "  ____) | |_| |_| | (_| | |_| |",
                                " |_____/ \\__|\\__,_|\\__,_|\\__, |",
                                "                          __/ |",
                                "                         |___/ "
                            ],
                            level3:[{
                                    name:"PJWSTK",
                                    desc:[
                                        "Polish-Japanese Institute of Information Technology",
                                        "MASTER OF SCIENCE - SPEC. DATABASE",
                                        "JAVA, .NET c#, MSSQL, ORACLE, DB2, db4o",
                                        "IX 2009 - VI 2012"
                                    ]
                                },
                                {
                                    name:"SGH",
                                    desc:[
                                        "Warsaw School of Economics",
                                        "POSTGRADUATE STUDIES IN MANAGEMENT INFORMATION SYSTEMS -STRATEGY, DESIGN, INTEGRATION",
                                        ".NET, MSSQL, OLAP, SAP (aSAP), BizzTalk",
                                        "IX 2004 - V 2005"         
                                    ]
                                },
                                {
                                    name:"JanskiAcademy",
                                    desc:[
                                        "The Bogdan Jański Academy",
                                        "BACHELOR OF MANAGEMENT & MARKETING",
                                        "Management, Internet Marketing",
                                        "IX 1999 - VI 2003"
                                    ]
                                },
                                {
                                    name:"Tem",
                                    desc:[
                                        "Technical School Computer Systems gen. Zajączka",
                                        "ELECTRONICS TECHNICIAN SPECIALIZATION COMPUTER SYSTEMS",
                                        "Computer Systems, I place in the National Tournament of Young Masters of Technology",
                                        "IX 1993 - VI 1998"
                                    ]
                                }
                                
                            ]
                            
                        },{
                            name:"Courses",
                            desc:[
"  .oooooo.                                                              ",
" d8P'  `Y8b                                                             ",
"888           .ooooo.  oooo  oooo  oooo d8b  .oooo.o  .ooooo.   .oooo.o ",
"888          d88' `88b `888  `888  `888\"\"8P d88(  \"8 d88' `88b d88(  \"8 ",
"888          888   888  888   888   888     `\"Y88b.  888ooo888 `\"Y88b.  ",
"`88b    ooo  888   888  888   888   888     o.  )88b 888    .o o.  )88b ",
" `Y8bood8P'  `Y8bod8P'  `V88V\"V8P' d888b    8\"\"888P' `Y8bod8P' 8\"\"888P' ",
""                                                                        
    ],
                            level3:[
                                {
                                    name :"M101JS",
                                    desc:["MongoDB for Node.js Developers"]
                                },
                                {
                                    name:"Microsoft2559",
                                    desc:[
                                        "Introduction to Visual Basic .NET Programming with Microsoft .NET 2073",
                                        "Programming a Microsoft SQL Server 2000 Database"
                                    ]
                                },
                                {
                                    name:"Microsoft2074",
                                    desc:[
                                        "Designing and Implementing OLAP Solutions Using Microsoft SQL Server 2000"
                                    ]
                                },
                                {
                                    name:"Microsoft2728",
                                    desc:[
                                        "Building Microsoft BizTalk Server 2002 Solutions"
                                    ]
                                },
                                {
                                    name:"SAP POLSKA",
                                    desc:["ASAP"]
                                }
                            ]
                            
                        }
                    ]
                },
                {
                  name:"eXperience",
                  level2:[
                      {
                       name:"Work",
                        desc:[
                            "projects over years:",
                            "             1998 □□□□□□□□□□□□□□□□ 2014",
                            "javascript:       ■■■□■□□■□■■■■■■■     ",
                            "asctionscript:    □□■■■■■■■■■■■■■■     ",
                            "php               □□■■□■□■■□□■■■■■     ",
                            ".NET              □□□□■■■■□■□■□■■■     ",
                            
                        ],
                       level3:[
                           {
                               name:"SeniorProgramer",
                               desc:[
                                   "AFG",
                                   "II-2013-IX-2014",
                                   "Company's Industry: Internet StartUp ",
                                   "PHP, JavaScript"
                               ]
                           },
                           {
                               name:"SeniorProgramer",
                               desc:[
                                   "WHITE-TELEKOM",
                                   "IV-2012 - I-2013",
                                   "Company's Industry: Internet StartUp",
                                   "PHP, JavaScript, ActionScript"
                               ]
                           },
                           {
                            name:"TeamLeader",
                            desc:[
                                "GALILEO MEDIA",
                                "III 2011 - III 2012",
                                "Company's Industry: Internet - E-Commerce - New Media",
                                "Project Managing, Coatching, Action Script, .NET, PHP, JavaScript"
                              ]
                          
                            },
                            {
                                name:"SeniorProgramer",
                                desc:[
                                    "GALILEO MEDIA",
                                    "VII 2008 - III 2011",
                                    "Company's Industry: Internet - E-Commerce - New Media",
                                    "Action Script, .NET, PHP, JavaScript"
                                ]
                            },
                            {
                                name:"TeamLeader",
                                desc:[
                                    "KOMBINAT MULTIMEDIANY - INTERACTIVE AGENCY",
                                    "V 2003 - VII 2008",
                                    "Company's Industry: Internet - E-Commerce - New Media",
                                    "php, Javascript, ActionScript"
                                ]
                            },
                            {
                                name:"TeamLeader",
                                desc:[
                                    "MARKETING & MEDIA SP. Z O.O. - FULL SERVICE AGENCY",
                                    "II 2000 - V 2003",
                                    "Company's Industry: Marketing - Public Relations",
                                    "php, Javascript, ActionScript)"
                                ]
                            },
                            {
                                name:"ProjectManager",
                                desc:[
                                    "SPEKTACLIC SC",
                                    "VI 1999 - II 2000",
                                    "Company's Industry: Internet - E-Commerce - New Media",
                                    "relations with clients, project management, programming (php, Javascript, ActionScript)"
                                ]
                            },
                            {
                                name:"HelpDesk",
                                desc:[
                                    "AGS NEW MEDIA",
                                    "XI 1998 – VI 1999",
                                    "Company's Industry: Internet - E-Commerce - New Media",
                                    "Windows installations, service workstations, LAN configuration etc."
                                ]
                            }
                       ]
                    },
                    {
                        name:"Skils",
                        desc:[
" oooooooo8 oooo        o88   o888              ",
"888         888  ooooo oooo   888   oooooooo8  ",
" 888oooooo  888o888     888   888  888ooooooo  ",
"        888 8888 88o    888   888          888 ",
"o88oooo888 o888o o888o o888o o888o 88oooooo88  "
                                               
                        ],
                        level3:[
                            {
                                name:"JavaScript",
                                desc:[
                                    "Vanilla – mostly some effects or computation",
                                    "jQuery (use with many plugins, I was write some plugins too)",
                                    "knockoutjs MVVM – bidirectional data binding",
                                    "node.js",
                                    "meteorjs"
                                ]
                            },{
                                name:"ActionScript",
                                desc:[
                                    "2d/3d application (Papervision)",
                                    "Remoting",
                                    "Games",
                                    "IOS/Android application"
                                ]
                            },
                            {
                                name:"Php",
                                desc:[
                                    "fatfree, symphony, joomla, modX, codeception, composer"
                                ]
                            },
                            {
                                name:"Java",
                                desc:[
                                    "Android applications",
                                    "Desktop aplications"
                                ]
                            },
                            {
                                name:"Ide",
                                desc:[
                                    "Netbeans",
                                    "Eclipse",
                                    "Git",
                                    "Mercurial",
                                    "Jenkins",
                                    "Ant",
                                ]
                            },
                            {
                                name:"Systems",
                                desc:[
                                    "Windows",
                                    "Linux (Ubuntu, Mint, CentOs)"
                                ]
                            },
                            {
                                name:"Languages",
                                desc:[
                                    "Polis - native",
                                    "English - comunicative"
                                ]
                            }
                        ]
                    }
                      
                  ]
                }
            ]
        }
        }
            
           
           
  


});
