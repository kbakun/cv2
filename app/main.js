// to depend on a bower installed component:
// define(['component/componentName/file'])

define(["jquery", "koExt","data","koMaping","handlebars"], function($, ko,data,mapping,Handlebars) {
  data.status = 'active';
  var vm = data;//mapping.fromJS(data);
  vm.lang = ko.observable(data.langs[1]);
  vm.cv = ko.computed(function(){
      var source=vm.lang().template;
      var template = Handlebars.compile(source);
      return template(data.cvs);
  });
  
  global = vm;
  ko.applyBindings(vm, $('html')[0]);
});
